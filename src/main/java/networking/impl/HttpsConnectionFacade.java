package networking.impl;

import networking.ConnectionFacade;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

// Ukrywa szczegóły klasy HttpsURLConnection pod naszą fasadą. Użytkownik naszej klasy
// nie musi znać wszystkich szczegółów konfiguracyjnych aby nawiązać połączenie.
public class HttpsConnectionFacade implements ConnectionFacade {
    private HttpsURLConnection connection;

    // Ukrywamy szczegóły dotyczące nawiązania połączenia.
    public void connect(String url) {
        try {
            URL _url = new URL(url);
            if (connection != null) {
                connection.disconnect();
            }
            connection = (HttpsURLConnection)_url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("User-Agent", "");
            if (connection.getResponseCode() != 200) {
                System.out.println("Connection failed with error code:: " + connection.getResponseCode());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Delegujemy wywołanie do klasy wewnętrznej.
    public void disconnect() {
        if (connection != null) {
            connection.disconnect();
        }
    }

    // Ukrywamy obsługę wyjatków.
    public InputStream getInputStream() {
        try {
            if (connection != null) {
                return connection.getInputStream();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
