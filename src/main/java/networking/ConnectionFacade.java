package networking;

import java.io.InputStream;

// Ułatwia i uogólnia połączenie
public interface ConnectionFacade {
    void connect(String url);
    void disconnect();
    InputStream getInputStream();
}
