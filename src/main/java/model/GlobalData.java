package model;

// Nasz model danych.
public class GlobalData {
    public int cases;
    public int deaths;
    public int recovered;
    public long updated;
    public int active;
    public int affectedCountries;
}
