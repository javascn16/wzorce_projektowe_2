import reader.impl.DataReaderImpl;
import ui.presenter.CoronaDataViewPresenter;
import ui.presenter.impl.CoronaDataViewPresenterImpl;
import ui.view.CoronaDataView;
import ui.view.impl.SwingCoronaDataView;

// Główna klasa aplikacji. Tutaj możemy zdefiniować konkrety takie jak,
// na przykład, rodzaj widoku (konsolowy albo swingowy).
public class CoronaTracker {
    private CoronaDataViewPresenter presenter;

    public void start() {
        CoronaDataView view = new SwingCoronaDataView();
        DataReaderImpl reader = new DataReaderImpl();
        presenter = new CoronaDataViewPresenterImpl(reader);
        presenter.setView(view);
    }
}
