package reader;

import observer.DataObserver;

// Uogólnienie czytania danych. Implementacje mogą czytać różne dane: z sieci,
// z pliku, z bazy danych, bezpośrednio z urządzeń (np. skaner). Szczegóły związane z
// odczytem występują na etapie implementacji i nie interesują użytkownika Readera.
// Jedyne co nas interesuje z punktu widzenia użytkownika to możliwość zapisania się
// na na zdarzenie i odebranie danych.
public interface DataReader {
    void addObserver(DataObserver observer);
    void removeObserver(DataObserver observer);
    void requestData();
}
