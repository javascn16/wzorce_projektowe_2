package reader.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.GlobalData;
import networking.ConnectionFacade;
import networking.impl.HttpsConnectionFacade;
import observer.DataObserver;
import reader.DataReader;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

//Klasa odpowiedzialna za odczyta danych z internetowego endpointa
//Element wzorca projektowego "obserwator".
public class DataReaderImpl implements DataReader {
    private static final String ENDPOINT_ADDRESS = "https://corona.lmao.ninja/all";

    private List<DataObserver> observers = new LinkedList<>();

    public void addObserver(DataObserver observer) {
        if (observer != null && !observers.contains(observer)) {
            observers.add(observer);
        }
    }

    public void removeObserver(DataObserver observer) {
        observers.remove(observer);
    }

    public void requestData() {
        readEndpoint();
    }

    // Odczytuje dane.
    private void readEndpoint() {
        // Nawiązanie połączenia.
        ConnectionFacade connection = new HttpsConnectionFacade();
        connection.connect(ENDPOINT_ADDRESS);
        // Pobranie strumienia danych z połączenia.
        InputStream dataStream = connection.getInputStream();
        // Utworzenie obiektu mapującego dane pochodzące z połączenia na nasz obiekt modelu.
        ObjectMapper mapper = new ObjectMapper();
        try {
            // Odczytanie wartości ze strumienia i zmapowanie na obiekt klasy GlobalData.
            GlobalData globalData = mapper.readValue(dataStream, GlobalData.class);
            for(DataObserver observer : observers) {
                observer.onData(globalData);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
