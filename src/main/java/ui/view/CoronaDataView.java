package ui.view;

// Uogólnienie widoku danych na temat pandemii.
// Widok wyświetla informacje nt. liczby przypadków (cases), liczby zgonów (deaths)
// oraz liczby wyzdrowiałych (recovered).
public interface CoronaDataView {
    void updateCases(String cases);
    void updateDeaths(String deaths);
    void updateRecovered(String recovered);
    void show();
}
