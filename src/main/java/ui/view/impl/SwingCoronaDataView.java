package ui.view.impl;

import ui.view.CoronaDataView;

import javax.swing.*;

// Implementacja widoku CoronaDataView wykorzystującą bibliotekę Swing.
public class SwingCoronaDataView implements CoronaDataView {
    private JFrame window;

    private JPanel casesPanel;
    private JPanel deathsPanel;
    private JPanel recoveredPanel;

    private JTextField cases;
    private JTextField deaths;
    private JTextField recovered;
    private JTextField casesLabel;
    private JTextField deathsLabel;
    private JTextField recoveredLabel;

    public SwingCoronaDataView() {
        // Tworzy okienko Swing
        window = new JFrame("COVID-19 Tracker");
        window.setLayout(new BoxLayout(window.getContentPane(), BoxLayout.PAGE_AXIS));

        // Tworzy pole tekstowe i panel
        cases = new JTextField();
        casesLabel = new JTextField("Cases:");
        casesPanel = new JPanel();
        // Dodaje pola tekstowe do panelu
        casesPanel.add(casesLabel);
        casesPanel.add(cases);
        // Ustawia styl rozmieszczania elementów panelu na horyzontalny.
        casesPanel.setLayout(new BoxLayout(casesPanel, BoxLayout.LINE_AXIS));

        deaths = new JTextField();
        deathsLabel = new JTextField("Deaths:");
        deathsPanel = new JPanel();
        deathsPanel.add(deathsLabel);
        deathsPanel.add(deaths);
        deathsPanel.setLayout(new BoxLayout(deathsPanel, BoxLayout.LINE_AXIS));

        recovered = new JTextField();
        recoveredLabel = new JTextField("Recovered:");
        recoveredPanel = new JPanel();
        recoveredPanel.add(recoveredLabel);
        recoveredPanel.add(recovered);
        recoveredPanel.setLayout(new BoxLayout(recoveredPanel, BoxLayout.LINE_AXIS));

        window.add(casesPanel);
        window.add(deathsPanel);
        window.add(recoveredPanel);

        window.setSize(600, 120);
        window.setLocation(100, 100);
    }

    @Override
    public void updateCases(String cases) {
        this.cases.setText(cases);
    }

    @Override
    public void updateDeaths(String deaths) {
        this.deaths.setText(deaths);
    }

    @Override
    public void updateRecovered(String recovered) {
        this.recovered.setText(recovered);
    }

    @Override
    public void show() {
        window.setVisible(true);
    }
}
