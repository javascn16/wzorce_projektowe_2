package ui.view.impl;

import ui.view.CoronaDataView;

// Implementacja widoku CoronaDataView wykorzystującą konsolę.
public class ConsoleCoronaDataView implements CoronaDataView {
    @Override
    public void updateCases(String cases) {
        System.out.println("Global cases: " + cases);
    }

    @Override
    public void updateDeaths(String deaths) {
        System.out.println("Global deaths: " + deaths);
    }

    @Override
    public void updateRecovered(String recovered) {
        System.out.println("Global recovered: " + recovered);
    }

    @Override
    public void show() {

    }
}
