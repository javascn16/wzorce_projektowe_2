package ui.presenter.impl;

import model.GlobalData;
import observer.DataObserver;
import reader.DataReader;
import ui.presenter.CoronaDataViewPresenter;
import ui.view.CoronaDataView;

public class CoronaDataViewPresenterImpl implements
        CoronaDataViewPresenter,
        DataObserver
{
    private CoronaDataView view;
    private DataReader reader;

    public CoronaDataViewPresenterImpl(DataReader reader) {
        if (reader == null) {
            throw new RuntimeException("DataReader cannot be null!");
        }
        this.reader = reader;
    }

    // Prezenter przyjmuje widok, następnie wyświetla go, rejestruje się jako obserwator
    // reader-a i zleca odczyt danych. Prezenter zawiera logikę.
    @Override
    public void setView(CoronaDataView view) {
        if (view == null) {
            return;
        }
        this.view = view;
        view.show();
        reader.addObserver(this);
        reader.requestData();
    }

    // Metoda obserwatora. Prezenter otrzymuje w niej nowe dane od reader-a
    // i przekazuje je do widoku.
    @Override
    public void onData(GlobalData data) {
        if (data == null) {
            return;
        }
        String casesStr = String.valueOf(data.cases);
        String deathStr = String.valueOf(data.deaths);
        String recoveredStr = String.valueOf(data.recovered);
        view.updateCases(casesStr);
        view.updateDeaths(deathStr);
        view.updateRecovered(recoveredStr);
    }
}
